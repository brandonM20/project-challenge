import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  constructor() { }

  ngOnInit(): void {
  }

  toHome(){
    document.getElementById("section-bienvenidos")?.scrollIntoView({behavior: "smooth"});
  }
  toSiguenos(){
    document.getElementById("section-siguenos")?.scrollIntoView({behavior: "smooth"});
  }
  toBeneficios(){
    document.getElementById("section-beneficios")?.scrollIntoView({behavior: "smooth"});
  }
}
