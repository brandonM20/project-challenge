import { Injectable } from '@angular/core';
import {CountryI, CityI} from '../models/countries.interface'

@Injectable()
export class CountriesService {
  private countries: CountryI[] = [
    {
      id: 1,
      name:'Argentina'
    },
    {
      id: 2,
      name:'Chile'
    },
    {
      id: 3,
      name:'Colombia'
    },
    {
      id: 4,
      name:'Mexico'
    },
    {
      id: 5,
      name:'Peru'
    }
  ];
  private provinces : CityI[] = [
    {
      id:1,
      countryId: 1,
      name: 'Buenos Aires'
    },
    {
      id:2,
      countryId: 1,
      name: 'Córdoba'
    },
    {
      id:3,
      countryId: 1,
      name: 'Santa Fe'
    },
    {
      id:4,
      countryId: 1,
      name: 'Mendoza'
    },
    {
      id:5,
      countryId: 1,
      name: 'Chaco'
    },
    {
      id:6,
      countryId: 2,
      name: 'Santiago'
    },
    {
      id:7,
      countryId: 2,
      name: 'La Serena'
    },
    {
      id:8,
      countryId: 2,
      name: 'Cuyo'
    },
    {
      id:9,
      countryId: 2,
      name: 'Chillán'
    },
    {
      id:10,
      countryId: 2,
      name: 'Concepción'
    },
    {
      id:11,
      countryId: 3,
      name: 'Bolívar'
    },
    {
      id:12,
      countryId: 3,
      name: 'Boyacá'
    },
    {
      id:13,
      countryId: 3,
      name: 'Caldas'
    },
    {
      id:14,
      countryId: 3,
      name: 'Cauca'
    },
    {
      id:15,
      countryId: 3,
      name: 'Magdalena'
    },
    {
      id:16,
      countryId: 4,
      name: 'Aguascalientes'
    },
    {
      id:17,
      countryId: 4,
      name: 'Michoacán'
    },
    {
      id:18,
      countryId: 4,
      name: 'Chihuahua'
    },
    {
      id:19,
      countryId: 4,
      name: 'Oaxaca'
    },
    {
      id:20,
      countryId: 4,
      name: 'Morelos'
    },
    {
      id:21,
      countryId: 5,
      name: 'Arequipa'
    },
    {
      id:22,
      countryId: 5,
      name: 'Cajamarca'
    },
    {
      id:23,
      countryId: 5,
      name: 'Cusco'
    },
    {
      id:24,
      countryId: 5,
      name: 'Lambayeque'
    },
    {
      id:25,
      countryId: 5,
      name: 'Lima'
    },
  ];
  getCountries(): CountryI[]{
    return this.countries;
  }
  getProvinces(): CityI[]{
    return this.provinces;
  }
}
