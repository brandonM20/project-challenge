import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { Observable } from 'rxjs';
import {Register} from '../../register/models/register.interface'

@Injectable({
  providedIn: 'root'
})
export class RegisterService {
url!: 'http://private-8e8921-woloxfrontendinverview.apiary-mock.com' 
  constructor(private http: HttpClient) { }

  postRegister(form: Register): Observable<Register>{
    let direccion = this.url + 'signup';
    return this.http.post<Register>(direccion, form)
  }
}
