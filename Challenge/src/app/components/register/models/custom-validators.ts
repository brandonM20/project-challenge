import { ValidationErrors, ValidatorFn, AbstractControl} from '@angular/forms'

export class CustomValidators{
    static patternValidator(regex: RegExp, error: ValidationErrors): ValidatorFn { 
        return (control: AbstractControl): { [key: string]: any} => {
            if(!control.value){
               return null as any;
            }

            const valid = regex.test(control.value);

            return valid ? null as any: error;
        };
    }

    static passwordMachtValidator(control: AbstractControl){
        const Password: string = control.get('Password')?.value;
        const Confirm: string = control.get('Confirm')?.value;

        if(Password !== Confirm){
            control.get('Confirm')?.setErrors({NoPasswordMatc: true})
        }
    }

    static numeric(control:  AbstractControl){
        let val = control.value;

        if(val === null || val === '') return null as any;

        if(!val.toString().match(/^[0-9]+(\.?[0-9]+)?$/)) return { 'invalidNumber': true}
    }
}