import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms'
import{ CountryI, CityI } from '../models/countries.interface'
import { CountriesService } from '../services/countries.service'
import{ RegisterService } from '../services/register.service'
import { CustomValidators} from '../models/custom-validators'
import { Register } from '../../register/models/register.interface'
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css'],
  providers: [CountriesService]
})
export class RegisterComponent implements OnInit {
  public selectedCountry: CountryI = {id: 0, name: ''};
  public countrie! : CountryI[];
  public province!: CityI[];
  userForm!: FormGroup;
  numberRegEx = /\-?\d*\.?\d{1,2}/;

  constructor(private countrieService: CountriesService,
              public  registerService: RegisterService,
              public formBuilder: FormBuilder) {}

private user: Register = {
    name: '',
    last_name: '',
    country: '',
    province: '',
    mail: '',
    password: '',
    phone: ''
  }

  ngOnInit(): void {
    
    this.countrie = this.countrieService.getCountries();

    this.userForm = this.formBuilder.group({
      firstName: ['', [Validators.required, Validators.minLength(5) ,Validators.maxLength(30)]],
      lastName: ['',  [Validators.required, Validators.minLength(5) ,Validators.maxLength(30)]],
      Country: ['', [Validators.required]],
      Province: ['', [Validators.required]],
      Email: ['', [Validators.required, Validators.email]],
      Number: ['', [Validators.required, Validators.maxLength(10), Validators.pattern(this.numberRegEx)]],
      Password: [null, Validators.compose([Validators.required, 
        CustomValidators.patternValidator(/\d/,{
        hasNumber: true
      }),
      CustomValidators.patternValidator(/[A-Z]/,{
        hasCapitalCase: true
      }),
      CustomValidators.patternValidator(/[a-z]/, {
        hasSmallCase: true
      }),
      CustomValidators.patternValidator(/[ !@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]/, {
        hasSpecialCharacters: true
      }
      ),
      Validators.minLength(6),
      Validators.maxLength(16)
      ])
    ],
      Confirm: [null, Validators.compose([Validators.required])],

      Accept: ['', [Validators.required]]
    },
    {
      validator: CustomValidators.passwordMachtValidator  
    }
    )
  }

  onRegister(form: Register): void{
    console.log(form)
  }

  localStorageData(){
    localStorage.setItem('firstName', JSON.stringify(this.userForm.value))

    JSON.parse(localStorage.getItem('user') || '{}')
  }

  onSelect(id: number):void{
    this.province = this.countrieService.getProvinces().filter(item => item.countryId == id)
  }
  get getControl(){
    return this.userForm.controls;
  }
  
  onSubmit(form: Register){
    this.registerService.postRegister(form).subscribe(res =>{
      console.log(res)
    })
  }
}