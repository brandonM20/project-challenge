import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ListService {

  constructor( private http: HttpClient ) { }

  getList(): Observable<any>{
    return this.http.get('http://private-8e8921-woloxfrontendinverview.apiary-mock.com/techs')
  }
}
