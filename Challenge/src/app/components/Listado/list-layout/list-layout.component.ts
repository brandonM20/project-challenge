import { Component, OnInit } from '@angular/core';
import { ListService } from '../services/list.service';
import { List } from '../models/list.interface';
@Component({
  selector: 'app-list-layout',
  templateUrl: './list-layout.component.html',
  styleUrls: ['./list-layout.component.css']
})
export class ListLayoutComponent implements OnInit {
  listProgram!: List[]
  filterPost!: '';

  constructor(private listService: ListService) { 
  }
  
  getOrderList(): void{
    this.listService.getList().subscribe(data=>{
      this.listProgram = data;
    })
  }

  ngOnInit(): void {
    this.getOrderList();
  }
  
  key: string = 'author'
  reverse: boolean = false;
  sort(key:any){
    this.key = key
    this.reverse = !this.reverse
  }

}
