import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms'
import {LoginService} from '../services/login.service'

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit { 

  constructor( ) { }

  myForm = new FormGroup({
    userName: new FormControl('', Validators.required),
    Password: new FormControl('', Validators.required),
});
  login() {
    // TODO: Use EventEmitter with form value
    console.warn(this.myForm.value);
  }

  ngOnInit(): void {
  }

}
