import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { Login} from '../models/login.interface'
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class LoginService {
  base_URL!: 'http://private-8e8921-woloxfrontendinverview.apiary-mock.com';

  constructor(private http: HttpClient) { }
  postLogin(login: Login): Observable<Login>{
    let direccion = this.base_URL + 'login';
    return this.http.post<Login>(direccion, login)
  }
}
