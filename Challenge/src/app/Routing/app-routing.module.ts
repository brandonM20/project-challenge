import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BeneficiosComponent } from '../components/beneficios/beneficios-layout/beneficios.component';
import { BienvenidosComponent } from '../components/bienvenidos/bienvenidos-layout/bienvenidos.component';
import { HomeLayoutComponent } from '../components/home/home-layout/home-layout.component';
import { ListLayoutComponent } from '../components/Listado/list-layout/list-layout.component';
import { LoginComponent } from '../components/login/login-layout/login.component';
import { RegisterComponent } from '../components/register/register-layout/register.component';
import { SiguenosComponent } from '../components/siguenos/siguenos-layout/siguenos.component';

const routes: Routes = [
  {path: '', component: HomeLayoutComponent},
  {path: 'bienvenidos', component: BienvenidosComponent},
  {path: 'siguenos', component: SiguenosComponent},
  {path:'beneficios', component: BeneficiosComponent},
  {path: 'login', component: LoginComponent},
  {path: 'register', component: RegisterComponent},
  {path: 'list' , component: ListLayoutComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
