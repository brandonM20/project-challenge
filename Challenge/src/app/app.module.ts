import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './Routing/app-routing.module';
import { AppComponent } from './app.component';
import { BeneficiosComponent } from './components/beneficios/beneficios-layout/beneficios.component';
import { FooterComponent  } from './components/footer/footer-layout/footer.component';
import { HeaderComponent } from './components/header/header-layout/header.component';
import { HomeLayoutComponent } from './components/home/home-layout/home-layout.component';
import { LoginComponent } from './components/login/login-layout/login.component';
import { RegisterComponent } from './components/register/register-layout/register.component';
import { SiguenosComponent } from './components/siguenos/siguenos-layout/siguenos.component';
import { BienvenidosComponent } from './components/bienvenidos/bienvenidos-layout/bienvenidos.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import {LoginService} from './components/login/services/login.service';
import {RegisterService} from './components/register/services/register.service';
import { HttpClientModule } from '@angular/common/http';
import { MatTableModule } from '@angular/material/table';
import { MatFormFieldModule } from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {MatListModule} from '@angular/material/list'
import { ListLayoutComponent } from './components/Listado/list-layout/list-layout.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FilterPipe } from './Pipes/filter.pipe';
import{ OrderModule } from 'ngx-order-pipe';


@NgModule({
  declarations: [
    AppComponent,
    BeneficiosComponent,
    FooterComponent,
    HeaderComponent,
    HomeLayoutComponent,
    LoginComponent,
    RegisterComponent,
    SiguenosComponent,
    BienvenidosComponent,
    ListLayoutComponent,
    FilterPipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule,
    MatTableModule,
    MatFormFieldModule,
    MatInputModule,
    MatListModule,
    BrowserAnimationsModule,
    OrderModule
  ],
  providers: [LoginService, RegisterService],
  bootstrap: [AppComponent]
})
export class AppModule { }
