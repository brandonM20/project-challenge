import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filter'
})
export class FilterPipe implements PipeTransform {

  transform(value: any, ...arg: any[]): any {
    const resultPost = [];
    for(const list of value){
      if(list.language.toLowerCase().indexOf(arg) > -1){
        resultPost.push(list);
      }if(list.type.toLowerCase().indexOf(arg)> -1){
        resultPost.push(list);
      }
    };
    return resultPost;
  }

}
