// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
   firebaseConfig : {
    apiKey: "AIzaSyCcYlYfr42HbDOCdZ2TLJb__OMwaMaEubs",
    authDomain: "challenge-proyect.firebaseapp.com",
    projectId: "challenge-proyect",
    storageBucket: "challenge-proyect.appspot.com",
    messagingSenderId: "198658824677",
    appId: "1:198658824677:web:0f52d610221217e905770c",
    measurementId: "G-8NSS576CGP"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
